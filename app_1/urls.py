from django.urls import path
from app_1.views import index, andijon, toshkent_1


urlpatterns = [
    path('', index, name='app_1_index'),
    path('t', toshkent_1, name='app_1_tosh'),
    path('a', andijon, name='app_1_and')
]
