from django.shortcuts import render

def index(request):
    return render(request, 'index.html')

def toshkent_1(request):
    return render(request, 'toshkent.html')

def andijon(request):
    return render(request, 'andijon.html')
